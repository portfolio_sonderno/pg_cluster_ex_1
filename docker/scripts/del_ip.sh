#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <ip_address>"
    exit 1
fi

ip_address=$1
default_interface=$(ip route | awk '/default/ {print $5}')
default_ip=$(ip addr show dev $default_interface | awk '/inet / {print $2}')

if ! [[ $ip_address =~  $default_ip ]]; then
    echo "IP address is not set on interface $default_interface"
    exit 1
else
    ip addr del $ip_address/16 dev $default_interface
    echo "IP address $ip_address removed from interface $default_interface"
    exit 0
fi